package com.khanas.model;

import com.khanas.annotation.MyAnnotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Human {

    private Logger logger = LogManager.getLogger();

    @MyAnnotation(name = "Roman")
    private String name;
    @MyAnnotation()
    private String surname;
    private int age;

    public Human(final String surname, final int age) {
        this.surname = surname;
        this.age = age;
    }

    public final void myMethod(final String a, final int... args) {
        logger.info("MyMethod(String a, int... args)");
        logger.info(a);
        for (int integer : args) {
            logger.info(integer);
        }
    }

    public final void myMethod(final String... args) {
        logger.info("MyMethod(String... args)");
        for (String string : args) {
            logger.info(string);
        }
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final String getSurname() {
        return surname;
    }

    public final void setSurname(final String surname) {
        this.surname = surname;
    }

    public final int getAge() {
        return age;
    }

    public final void setAge(final int age) {
        this.age = age;
    }
}
