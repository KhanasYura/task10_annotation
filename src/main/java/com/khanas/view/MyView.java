package com.khanas.view;

import com.khanas.annotation.MyAnnotation;
import com.khanas.model.Human;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Logger logger = LogManager.getLogger();
    private Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> menuMethod;

    public MyView() {

        menu = new LinkedHashMap<>();
        menu.put("1", "1.Task1");
        menu.put("2", "2.Task2");
        menu.put("3", "3.Task3");
        menu.put("4", "4.Task4");
        menu.put("Q", "Q.Exit");

        menuMethod = new LinkedHashMap<>();
        menuMethod.put("1", this::task1);
        menuMethod.put("2", this::task2);
        menuMethod.put("3", this::task3);
        menuMethod.put("4", this::task4);
        show();

    }

    private void task1() {
        logger.info("-----------Field with annotation-------------");
        Class<Human> clazz = Human.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                MyAnnotation myAnnotation = field
                        .getAnnotation(MyAnnotation.class);
                logger.info("Field: " + field.getName());
                logger.info("Nama in @MyAnnotation: "
                        + myAnnotation.name());
                logger.info("Surname in @MyAnnotation: "
                        + myAnnotation.surname());
            }
        }
    }

    private void task2() {
        Human human = new Human("Petro", 26);
        getInfoAboutClass(human);
    }

    private void getInfoAboutClass(Object object) {
        try {
            Class<?> myClazz = object.getClass();
            logger.info("Class name: " + myClazz.getName());

            Field[] fields = myClazz.getDeclaredFields();
            for (Field field : fields) {
                logger.info("Field name: " + field.getName());
                Annotation[] annotations = field.getAnnotations();
                for (Annotation annotation : annotations) {
                    logger.info("Annotation name: "
                            + annotation.getClass().getSimpleName());
                }
            }

            Method[] methods = myClazz.getDeclaredMethods();
            for (Method method : methods) {
                logger.info("Method name: " + method.getName());
                Annotation[] annotations = method.getAnnotations();
                for (Annotation annotation : annotations) {
                    logger.info("Annotation name: "
                            + annotation.getClass().getName());
                }
            }

            logger.info("Invoke 3 methods(getAge, setName, getName)");

            Method methodToInvokeAge = myClazz.getMethod("getAge");
            methodToInvokeAge.setAccessible(true);
            logger.info(methodToInvokeAge.invoke(object));

            logger.info("Name before invocation: " + object.getClass().getName());
            Method methodToInvokeName = myClazz.getMethod("setName",
                    String.class);
            methodToInvokeName.setAccessible(true);
            methodToInvokeName.invoke(object, "Ivan");

            Method methodToInvokeName2 = myClazz
                    .getDeclaredMethod("getName");
            methodToInvokeName2.setAccessible(true);
            logger.info("Name after invocation:" + methodToInvokeName2.invoke(object));


        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    private void task3() {
        try {
            Human human = new Human("Petro", 26);
            Field[] fields = human.getClass().getDeclaredFields();
            fields[0].setAccessible(true);

            logger.info("Field value before:" + fields[0].get(human));

            if (fields[0].getType() == int.class) {
                fields[0].setInt(human, 28);
            }

            if (fields[0].getType() == String.class) {
                fields[0].set(human, "Ivan");
            }

            logger.info("Field value after:" + fields[0].get(human));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void task4() {
        try {
            Human human = new Human("Petro", 26);

            String[] arr = {"Oleg", "Eric", "Petro", "Ivan", "Yura"};
            Method method = human.getClass()
                    .getDeclaredMethod("myMethod", String[].class);
            method.invoke(human, new Object[]{arr});

            String string = "Some values: ";
            int[] integers = new int[]{1, 4, 7, 3, 9, 1};
            Method method2 = human.getClass()
                    .getDeclaredMethod("myMethod",
                            new Class[]{String.class, int[].class});
            method2.invoke(human, string, integers);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void outputMenu() {
        logger.info("MENU:");

        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    private void show() {
        String keyMenu;

        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();

            if (keyMenu.equals("Q")) {
                break;
            }

            menuMethod.get(keyMenu).print();
        } while (true);
    }
}
